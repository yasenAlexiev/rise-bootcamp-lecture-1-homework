﻿using System;
using System.Collections.Generic;

using NatureReserveSimulation.Foods;

namespace NatureReserveSimulation.Animals
{
    public class Animal
    {
        private int energy;
        private HashSet<Food> diet1;

        public string Name { get; set; }
        public int MaxEnergy { get; set; }
        public HashSet<Food> Diet { get; set; }
        public int CurrentEnergy { get; set; }
        public int Lifespan { get; set; }

        public Animal(string name, int maxEnergy, HashSet<Food> diet)
        {
            Name = name;
            MaxEnergy = maxEnergy;
            CurrentEnergy = maxEnergy;
            Diet = diet;
            Lifespan = 0;
          
        }

        public Animal(int energy, HashSet<Food> diet1, string name)
        {
            this.energy = energy;
            this.diet1 = diet1;
            Name = name;
        }

        public virtual string Feed(Animal animal, Food currentFood)
        {
            if (IsActive())
            {
                if (Diet.Contains(currentFood) && CurrentEnergy < MaxEnergy && currentFood.Count > 0)
                {
                    CurrentEnergy += currentFood.NutritionalValue;
                    currentFood.NutritionalValue -= 2;
                    currentFood.Count--;

                    if (!animal.IsActive())
                    {
                        return animal.DeathMessage(animal);
                    }

                    return $"{animal.Name} eats {currentFood.name}. {animal.Name}'s energy is now {CurrentEnergy}.";
                }
                else if (!Diet.Contains(currentFood))
                {
                    NotEat(animal, currentFood);
                }
                else
                {
                    NotEat(animal, currentFood);

                    if (!animal.IsActive())
                    {
                        return animal.DeathMessage(animal);
                    }
                }

                Lifespan++;
            }
            else
            {
                return DeathMessage(this);
            }

            return "";
        }

        public virtual void NotEat(Animal animal, Food food)
        {
            bool notEat = animal.Diet.Contains(food);

            if (food.Count == 0)
            {
                animal.CurrentEnergy--;
                Console.WriteLine($"{food.name} is empty. {animal.Name}'s energy is now {animal.CurrentEnergy}.");
            }
            else if (!notEat)
            {
                animal.CurrentEnergy--;
                Console.WriteLine($"{animal.Name} does not eat {food.name}. {animal.Name}'s energy is now {animal.CurrentEnergy}.");
            }
            else
            {
                animal.CurrentEnergy--;
                Console.WriteLine($"{animal.Name} is not hungry. {animal.Name}'s energy is now {animal.CurrentEnergy}.");
            }

            if (!animal.IsActive())
            {
                Console.WriteLine(animal.DeathMessage(animal));
            }
        }

        public bool IsActive()
        {
            return CurrentEnergy > 0;
        }

        public virtual string DeathMessage(Animal animal)
        {
            return $"{animal.Name} has died. :(";
        }

        internal void Feed(Food randomFoot)
        {
            throw new NotImplementedException();
        }
    }
}
