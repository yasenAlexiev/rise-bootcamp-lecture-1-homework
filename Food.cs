﻿namespace NatureReserveSimulation.Foods
{
    public abstract class Food
    {
        public string name;
        protected const int defaultNutrioneValue = 2;

        public int NutritionalValue { get; set; }
        public int Count { get; set; }


        public Food(string name, int nutritioneValue, int count)
        {
            this.name = name;
            this.NutritionalValue = nutritioneValue;
            this.Count = count;
        }


        public int Eat(int currentEnergy)
        {
            NutritionalValue -= currentEnergy;

            if (NutritionalValue <= 0)
            {
                Count--;
                NutritionalValue = defaultNutrioneValue;
            }

            return Count;
        }

        public override int GetHashCode()
        {
            return name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!(obj is Food)) return false;
            Food food = (Food)obj;

            return name.Equals(food.name);
        }
    }
}

