﻿using System;
using System.Collections.Generic;

public class TreeNode
{
    public int val;
    public TreeNode left;
    public TreeNode right;
    public TreeNode(int val = 0, TreeNode left = null, TreeNode right = null)
    {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}

public class Solution
{
    public int KthSmallest(TreeNode root, int k)
    {
        List<int> list = new List<int>();
        List<int> vals = list;
        AddVal(root, k, vals);
        return vals[k - 1];
    }

    private static void AddVal(TreeNode node, int k, List<int> vals)
    {
        if (node == null || vals.Count > k - 1)
        {
            return;
        }

        AddVal(node.left, k, vals);
        vals.Add(node.val);
        AddVal(node.right, k, vals);
    }
}

public class Program
{
    static void Main(string[] args)
    {
        // Create a sample binary tree
        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(1);
        root.right = new TreeNode(4);
        root.left.right = new TreeNode(2);

        // Find the 2nd smallest value
        Solution sol = new Solution();
        int kthSmallest = sol.KthSmallest(root, 2);

        Console.WriteLine($"The 2nd smallest value is: {kthSmallest}");
    }
}
