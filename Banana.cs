﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureReserveSimulation.Foods
{
    internal class  Banana: Food
    {
        public Banana() : base("banana", 2, 3)
        {
        }
    }

}
