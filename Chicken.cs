﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NatureReserveSimulation.Foods;

namespace NatureReserveSimulation.Animals
{
    public class Chicken : Animal
    {
        private const string name = "Chicken";
        private const int Energy = 4;

        static HashSet<Food> diet = new HashSet<Food>()
        {
            new Fruits()
        };

        public Chicken()
            : base(Energy, Diet1, name)
        {
        }

        public static HashSet<Food> Diet1 { get => diet; set => diet = value; }

        public override string DeathMessage(Animal animal)
        {
            return $"{Chicken.name} is in a better place now ";
        }
    }
}
