﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Animal;

namespace Main
{
    internal class MainLogic
    { 
    static void Main(string[] args)
    {
        List<Animal> animals = new List<Animal>();

        animals.Add(new Chicken("Joro"));
        animals.Add(new Chicken("Gregorii"));
        animals.Add(new Chicken("Haus"));

        animals.Add(new Wolf("Ivan"));
        animals.Add(new Wolf("Mitko"));
        animals.Add(new Wolf("Vasko"));

        animals.Add(new Lamb("Kircho"));
        animals.Add(new Lamb("Teodor"));
        animals.Add(new Lamb("Panchev"));

        animals.Add(new Beaver("Bongo"));
        animals.Add(new Beaver("Bingo"));
        animals.Add(new Beaver("Bango"));

        while (animals.Count > 0)
        {
            Console.WriteLine("---- Turn ----");
            foreach (Animal animal in animals.ToList())
            {
                animal.EnergyGain();
                if (animal.NumInstances <= 0)
                {
                    Console.WriteLine("A species has gone extinct");
                    animals.Remove(animal);
                }
            }
            Console.WriteLine("--------------\n");
        }

        Console.WriteLine("All animals are dead :(");

    }
}
}
}
