﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Event;
using Animal;
using Main;


namespace AnimalSimulation
{
    using System;

    namespace AnimalSimulation
    {
        class AnimalUI
        {
            private AnimalUI animal;

            public AnimalUI(Animal animal)
            {
                this.animal = animal ?? throw new ArgumentNullException(nameof(animal));
            }

            public void Display()
            {
                Console.WriteLine("-----------------------------");
                Console.WriteLine("Animal Details:");
                Console.WriteLine("Name: " + animal.Name);
                Console.WriteLine("Type: " + animal.Type);
                Console.WriteLine("Age: " + animal.Age);

                if (animal is Mammal)
                {
                    Console.WriteLine("Is Mammal: Yes");
                    Mammal mammal = (Mammal)animal;
                    Console.WriteLine("Nursing: " + (mammal.Nursing ? "Yes" : "No"));
                }
                else
                {
                    Console.WriteLine("Is Mammal: No");
                }

                if (animal is Bird)
                {
                    Console.WriteLine("Is Bird: Yes");
                    Bird bird = (Bird)animal;
                    Console.WriteLine("Can Fly: " + (bird.CanFly ? "Yes" : "No"));
                }
                else
                {
                    Console.WriteLine("Is Bird: No");
                }

                if (animal is Fish)
                {
                    Console.WriteLine("Is Fish: Yes");
                    Fish fish = (Fish)animal;
                    Console.WriteLine("Can Swim: " + (fish.CanSwim ? "Yes" : "No"));
                }
                else
                {
                    Console.WriteLine("Is Fish: No");
                }

                Console.WriteLine("-----------------------------");
            }

            public void OnAnimalBirth()
            {
                Console.WriteLine(animal.Name + " gave birth!");
            }

            public void OnAnimalDeath()
            {
                Console.WriteLine(animal.Name + " has died.");
            }

            public void OnAnimalHungry()
            {
                Console.WriteLine(animal.Name + " is hungry. Please feed them.");
            }

            public void OnAnimalFull()
            {
                Console.WriteLine(animal.Name + " is full. Thank you for feeding them.");
            }

            public void OnAnimalSleeping()
            {
                Console.WriteLine(animal.Name + " is sleeping.");
            }

            public void OnAnimalWakeUp()
            {
                Console.WriteLine(animal.Name + " woke up.");
            }

            public void OnAnimalMakeSound()
            {
                Console.Beep(500, 1000);
                Console.WriteLine(animal.Name + " made a sound.");
            }
        }
    }

}
