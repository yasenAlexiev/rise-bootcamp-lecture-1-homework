﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NaturalHabitatModel
{
    internal class Program
    {
        abstract class Animal
        {
            public string[] animalFood = new string[] { "peaches", "apples", "hay", "banana", "worm", "strawberry" };
            public int NumInstances { get; set; } = 5;
            public int Lifespan { get; set; } = 0;
            public int MaxEnergy { get; set; } = 10;
            public int Energy { get; set; } = 5;
            public string Name { get; set; }
            protected Dictionary<string, int> foodEnergy = new Dictionary<string, int>
            {
                { "peaches", 2 },
                { "apples", 3 },
                { "hay", 1 },
                { "banana", 2 },
                { "worm", 1 },
                { "strawberry", 3 }
            };

            public void Eat(string food)
            {
                if (animalFood.Contains(food))
                {
                    int energyToAdd = 0;
                    if (foodEnergy.TryGetValue(food, out energyToAdd))
                    {
                        Energy += energyToAdd;
                        if (Energy > MaxEnergy)
                        {
                            Energy = MaxEnergy;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Error: unknown food type: " + food);
                    }

                    Console.WriteLine(GetType().Name + " " + Name + " is eating " + food + " and gaining " + energyToAdd + " energy");
                    Lifespan++;
                }
                else
                {
                    Console.WriteLine(GetType().Name + " " + Name + " has died due to eating " + food);
                    NumInstances--;
                }

                if (Energy <= 0)
                {
                    Console.WriteLine(GetType().Name + " " + Name + " has died due to starvation");
                    NumInstances--;
                }
            }

            public void Update()
            {
                Energy--;
                if (Energy <= 0)
                {
                    Console.WriteLine(GetType().Name + " " + Name + " has died due to starvation");
                    NumInstances--;
                }
            }
        }

        class Chicken : Animal
        {
            public Chicken(string name)
            {
                Name = name;
                MaxEnergy = 6;
                Energy = MaxEnergy;
            }
        }

        class Wolf : Animal
        {
            public Wolf(string name)
            {
                Name = name;
                MaxEnergy = 8;
                Energy = MaxEnergy;
            }
        }

        class Lamb : Animal
        {
            public Lamb(string name)
            {
                Name = name;
                MaxEnergy = 6;
                Energy = MaxEnergy;
            }
        }

        class Beaver : Animal
        {
            public Beaver(string name)
            {
                Name = name;
                MaxEnergy = 7;
                Energy = MaxEnergy;
            }
        }

        class Animals
        {
            public static void Main()
            {
                List<Animal> animalList = new List<Animal>()
                {
                    new Chicken("George"), new Chicken("Martha"), new Chicken("Henry"), new Chicken("Abigail"), new Chicken("John"),
                    new Wolf("Scar"), new Wolf("Luna"), new Wolf("Midnight"), new Wolf("Blade"), new Wolf("Aurora"),
                    new Lamb("Sheepie"), new Lamb("Lamby"), new Lamb("Lambo"), new Lamb("Lamboozle"), new Lamb("Lambchop"),
                    new Beaver("Barry"), new Beaver("Betty"), new Beaver("Bongo"), new Beaver("Bango"), new Beaver("Bingo") };
                                while (animalList.Any())
                {
                    Animal currentAnimal = animalList.ElementAt(0);
                    currentAnimal.Update();
                    if (currentAnimal.Energy <= 0)
                    {
                        animalList.Remove(currentAnimal);
                    }
                    else
                    {
                        int foodIndex = new Random().Next(currentAnimal.animalFood.Length);
                        currentAnimal.Eat(currentAnimal.animalFood[foodIndex]);
                        if (currentAnimal.NumInstances <= 0)
                        {
                            animalList.Remove(currentAnimal);
                        }
                    }
                }

                Console.WriteLine("All animals have died. The end.");
            }
        }
    }
}
