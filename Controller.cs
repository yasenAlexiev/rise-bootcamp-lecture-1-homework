﻿using NatureReserveSimulation.Animals;
internal class Program
{
    static void Main(string[] args)
    {

        // Create a simulation with these parameters
        var simulation = new Simulator();

        // Run the simulation
        simulation.Run();
    }
}
