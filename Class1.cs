﻿using NatureReserveSimulation.Foods;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NatureReserveSimulation.Animals
{
    public class Simulator
    {
        private readonly List<Animal> _animals;
        private readonly List<Food> _foods;

        public Simulator()
        {
            _animals = new List<Animal>
            {
                new Bear(),
                new Sheep(),
                new Capybara(),
                new Chicken()
            };

            _foods = new List<Food>
            {
                new Banana(),
                new Fish(),
                new Fruits(),
                new Grass(),
                new Croissant()
            };
        }

        private Food GetRandomFood()
        {
            int randomIndex = new Random().Next(_foods.Count);
            return _foods[randomIndex];
        }

        private void RegenerateFood(int simulateDay)
        {
            if (simulateDay % 3 == 0)
            {
                foreach (var food in _foods)
                {
                    food.NutritionalValue++;
                }
            }
        }

        private bool HasAliveAnimal()
        {
            return _animals.Any(a => a.IsActive());
        }

        public void Simulate()
        {
            int simulateDay = 0;

            while (HasAliveAnimal())
            {
                Food randomFood = GetRandomFood();

                foreach (var animal in _animals)
                {
                    if (!animal.IsActive())
                    {
                        Console.WriteLine(animal.DeathMessage(animal));
                        continue;
                    }

                    animal.Feed(randomFood);
                }

                Console.WriteLine();

                simulateDay++;
                RegenerateFood(simulateDay);
            }
        }

        public void Run()
        {
            Simulate();

            int[] lifespans = _animals.Where(a => a.Lifespan >= 0).Select(a => a.Lifespan).ToArray();
            if (lifespans.Length > 0)
            {
                int minLifespan = lifespans.Min();
                int maxLifespan = lifespans.Max();
                double avgLifespan = lifespans.Average();

                Console.WriteLine($"Minimum lifespan: {minLifespan}");
                Console.WriteLine($"Maximum lifespan: {maxLifespan}");
                Console.WriteLine($"Average lifespan: {avgLifespan}");
            }
            else
            {
                Console.WriteLine("No animals with lifespans");
            }
        }
    }
}
