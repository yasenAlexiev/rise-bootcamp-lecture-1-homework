﻿using NatureReserveSimulation.Foods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NatureReserveSimulation.Animals
{
    public class Capybara : Animal
    {
        private const string name = "Capybara";
        private const int Energy = 7;

        static HashSet<Food> diet = new HashSet<Food>()
        {
            new Fruits()
        };

        public Capybara()
            : base(Energy, diet, name)
        {
        }


        public override string DeathMessage(Animal animal)
        {
            return $"{Capybara.name} is in a better place now :( ";
        }
    }
}

