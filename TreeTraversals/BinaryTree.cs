﻿using System;
using TreeTraversal;

     public class Program
{
    public static void Main()
    {
        BinaryTree tree = new BinaryTree();
        tree.Insert(24);
        tree.Insert(11);
        tree.Insert(25);
        tree.Insert(35);
        tree.Insert(38);
        tree.Insert(52);
        tree.Insert(61);
        tree.Insert(69);
        tree.Insert(76);

        Console.WriteLine("InOrder Traversal:");
        tree.InOrderTraversal(tree.root);

        Console.WriteLine("\nPreOrder Traversal:");
        tree.PreOrderTraversal(tree.root);

        Console.WriteLine("\nPostOrder Traversal:");
        tree.PostOrderTraversal(tree.root);
    }
}
