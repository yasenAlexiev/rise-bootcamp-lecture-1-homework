﻿using System;

namespace TreeTraversal
{
    public class BinaryTree
    {
        public Node root;

        public BinaryTree()
        {
            root = null;
        }

        public void Insert(int val)
        {
            Node newNode = new Node(val);

            if (root == null)
            {
                root = newNode;
                return;
            }

            Node current = root;

            while (true)
            {
                if (val < current.value)
                {
                    if (current.left == null)
                    {
                        current.left = newNode;
                        break;
                    }
                    else
                    {
                        current = current.left;
                    }
                }
                else
                {
                    if (current.right == null)
                    {
                        current.right = newNode;
                        break;
                    }
                    else
                    {
                        current = current.right;
                    }
                }
            }
        }

        public void InOrderTraversal(Node node)
        {
            if (node != null)
            {
                InOrderTraversal(node.left);
                Console.Write(node.value + " ");
                InOrderTraversal(node.right);
            }
        }

        public void PreOrderTraversal(Node node)
        {
            if (node != null)
            {
                Console.Write(node.value + " ");
                PreOrderTraversal(node.left);
                PreOrderTraversal(node.right);
            }
        }

        public void PostOrderTraversal(Node node)
        {
            if (node != null)
            {
                PostOrderTraversal(node.left);
                PostOrderTraversal(node.right);
                Console.Write(node.value + " ");
            }
        }
    }
}