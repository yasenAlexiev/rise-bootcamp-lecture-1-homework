﻿using System;
using System.Collections.Generic;

public class Program
{
    public static void Main()
    {
        Console.Write("Enter the number of disks: ");
        int n = int.Parse(Console.ReadLine());

        HanoiTowersSolver.Solve(n);
    }
}

public class HanoiTowersSolver
{
    public static void Solve(int n)
    {
        Stack<int> startPeg = new Stack<int>();
        Stack<int> intermediatePeg = new Stack<int>();
        Stack<int> destinationPeg = new Stack<int>();

        for (int i = n; i > 0; i--)
        {
            startPeg.Push(i);
        }

        MoveDisks(n, startPeg, destinationPeg, intermediatePeg);
    }

    private static void MoveDisks(int n, Stack<int> startPeg, Stack<int> destinationPeg, Stack<int> intermediatePeg)
    {
        if (n == 1)
        {
            destinationPeg.Push(startPeg.Pop());
            PrintPegs(startPeg, intermediatePeg, destinationPeg);
        }
        else
        {
            MoveDisks(n - 1, startPeg, intermediatePeg, destinationPeg);
            destinationPeg.Push(startPeg.Pop());
            PrintPegs(startPeg, intermediatePeg, destinationPeg);
            MoveDisks(n - 1, intermediatePeg, destinationPeg, startPeg);
        }
    }

    private static void PrintPegs(Stack<int> startPeg, Stack<int> intermediatePeg, Stack<int> destinationPeg)
    {
        Console.WriteLine("Start Peg: " + string.Join(" ", startPeg));
        Console.WriteLine("Intermediate Peg: " + string.Join(" ", intermediatePeg));
        Console.WriteLine("Destination Peg: " + string.Join(" ", destinationPeg));
        Console.WriteLine
//where's the rest of the file?