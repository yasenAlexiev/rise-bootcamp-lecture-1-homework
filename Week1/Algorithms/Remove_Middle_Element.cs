﻿using System;
using System.Collections.Generic;

public class Program
{
    public static void Main()
    {
        LinkedList<int> list = new LinkedList<int>();
        list.AddLast(1);
        list.AddLast(2);
        list.AddLast(3);
        list.AddLast(4);
        list.AddLast(5);

        Console.WriteLine("Original list:");
        foreach (int value in list)
        {
            Console.Write(value + " ");
        }
        Console.WriteLine();

        EraseMiddleElement(list);

        Console.WriteLine("List after erasing middle element:");
        foreach (int value in list)
        {
            Console.Write(value + " ");
        }
        Console.WriteLine();
    }

    public static void EraseMiddleElement(LinkedList<int> list)
    {
        // add checks for imput parameters.
        // Maybe return true if success and false if not
        int count = list.Count;
        if (count % 2 == 0)
        {
            // When number of elements is even
            int middleIndex = count / 2 - 1;
            // these next 6 lines are the same in both sections. take them out of the if
            LinkedListNode<int> middleNode = list.First;
            for (int i = 0; i < middleIndex; i++)
            {
                middleNode = middleNode.Next;
            }
            list.Remove(middleNode);
        }
        else
        {
            // When number of elements is odd
            int middleIndex = count / 2;
            LinkedListNode<int> middleNode = list.First;
            for (int i = 0; i < middleIndex; i++)
            {
                middleNode = middleNode.Next;
            }
            list.Remove(middleNode);
        }
    }
}
