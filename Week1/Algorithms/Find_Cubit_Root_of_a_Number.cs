﻿using System;

namespace Root
{
    public class CubeRoot
    {
        public static void Main(string[] args)
        {
            int cuberoot, result;

            Console.WriteLine("Enter a number to find its cubic root:");
            cuberoot = Convert.ToInt32(Console.ReadLine());

            result = (int)Math.Pow(cuberoot, 1.0 / 3.0);
            Console.WriteLine("Cube root is: " + result);
            Console.ReadKey();
        }
    }
}
