﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        // Define a list of words to group by anagrams
        List<string> words = new List<string> { "eat", "tea", "tan", "ate", "nat", "bat" };

        // Define an empty dictionary to store the anagram groups
        Dictionary<string, List<string>> anagramGroups = new Dictionary<string, List<string>>();

        // Loop through each word in the list
        foreach (string word in words)
        {
            // Sort the letters of the word alphabetically
            char[] letters = word.ToCharArray();
            Array.Sort(letters);
            string sortedWord = new string(letters);

            // Check if the sorted word is already in the dictionary
            if (anagramGroups.ContainsKey(sortedWord))
            {
                // If it is, add the word to the list of anagrams
                anagramGroups[sortedWord].Add(word);
            }
            else
            {
                // If it's not, create a new list for the anagram group and add the word
                List<string> group = new List<string>();
                group.Add(word);
                anagramGroups.Add(sortedWord, group);
            }
        }

        // Print the anagram groups to the console
        Console.WriteLine("Anagram groups:");
        foreach (List<string> group in anagramGroups.Values)
        {
            Console.WriteLine("[" + string.Join(", ", group) + "]");
        }
    }
}
