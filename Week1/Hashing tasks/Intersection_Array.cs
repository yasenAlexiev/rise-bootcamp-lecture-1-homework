﻿using System;
using System.Collections.Generic;

public class Program
{
    public int[] Intersection(int[] nums1, int[] nums2)
    {
        var hSet = new HashSet<int>();
        var list = new List<int>();
        foreach (var num in nums1)
        {
            hSet.Add(num);
        }

        foreach (var num in nums2)
        {
            if (hSet.Contains(num))
            {
                hSet.Remove(num);
                list.Add(num);
            }
        }

        return list.ToArray();
    }

    public static void Main(string[] args)
    {
        int[] nums1 = { 1, 2, 2, 1 };
        int[] nums2 = { 2, 2 };

        Program program = new Program();
        int[] intersection = program.Intersection(nums1, nums2);

        Console.WriteLine("Intersection of nums1 and nums2:");
        foreach (int num in intersection)
        {
            Console.WriteLine(num);
        }
    }
}
