﻿using NatureReserveSimulation.Foods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureReserveSimulation.Foods
{
    internal class Fish : Food
    {
        public Fish() : base("fish", 2, 1)
        {
        }
    }

}
