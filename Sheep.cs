﻿using NatureReserveSimulation.Foods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureReserveSimulation.Animals
{
    public class Sheep : Animal
    {
        private const string name = "Sheep";
        private const int Energy = 5;

        private HashSet<Food> diet;

        public Sheep(List<Food> food)
            : base(Energy, new HashSet<Food>(food), name)
        {
            this.diet = new HashSet<Food>(food);
        }

        public override string DeathMessage(Animal animal)
        {
            return $"{Sheep.name} is in a better place now ";
        }
    }
}
