﻿using NatureReserveSimulation.Foods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NatureReserveSimulation.Foods
{
    internal class Grass : Food
    {
        public Grass() : base("grass", 3, 2)
        {
        }
    }

}