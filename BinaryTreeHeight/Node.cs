﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Binary_Tree_Height
{
    class Node
    {
        public int value;
        public Node left;
        public Node right;

        public Node(int val)
        {
            value = val;
            left = null;
            right = null;
        }
    }
}

