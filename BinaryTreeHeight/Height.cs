﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Binary_Tree_Height
{
    class Height
    {
        public static int getHeight(Node root)
        {
            int height = -1;

            if (root == null)
            {
                return height;
            }
            else
            {
                height = 1 + Math.Max(getHeight(root.left), getHeight(root.right));
            }

            return height;
        }
    }
}

