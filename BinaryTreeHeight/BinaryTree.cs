﻿using System;
using System.Xml.Linq;

namespace Binary_Tree_Height
{
    internal class BinaryTree
    {
        public Node root;

        public BinaryTree()
        {
            root = null;
        }

        public void Insert(int val)
        {
            Node newNode = new Node(val);

            if (root == null)
            {
                root = newNode;
                return;
            }

            Node current = root;

            while (true)
            {
                if (val < current.value)
                {
                    if (current.left == null)
                    {
                        current.left = newNode;
                        break;
                    }
                    else
                    {
                        current = current.left;
                    }
                }
                else
                {
                    if (current.right == null)
                    {
                        current.right = newNode;
                        break;
                    }
                    else
                    {
                        current = current.right;
                    }
                }
            }
        }
    }
    }
    

