﻿using Binary_Tree_Height;
using System;


namespace Binary_Tree_Height
{

    public class Program
    {
        public static void Main()
        {
            BinaryTree tree = new BinaryTree();
            tree.Insert(24);
            tree.Insert(11);
            tree.Insert(25);
            tree.Insert(35);
            tree.Insert(38);
            tree.Insert(52);
            tree.Insert(61);
            tree.Insert(69);
            tree.Insert(76);
            // Calculate the height of the binary tree
            int height = getHeight(tree.root);
            Console.WriteLine("The height of the binary tree is: " + height);
        }

        private static int getHeight(Node root)
        {
            int height = -1;

            if (root == null)
            {
                return height;
            }
            else
            {
                height = 1 + Math.Max(getHeight(root.left), getHeight(root.right));
            }

            return height;
        }


    }
}
